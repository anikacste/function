<?php
if (!is_int(27.25)) {
    echo "is not int\n";
} else {
    echo "is int\n";
}
var_dump(is_int('abc'));
var_dump(is_int(23));
var_dump(is_int(23.5));
var_dump(is_int(1e7));  //Scientific Notation
var_dump(is_int(true));
?>