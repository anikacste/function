<?php
$foo = "5"; // string
$bar = true;   // boolean


settype($foo, "boolean");
settype($bar, "string");
echo gettype($foo);
echo gettype($bar);// $bar is now "1" (string)
?>